/*!\file shell.c
 * \brief A Documented source file.
 *
 *       Filename:  shell.c
 *
 *    Description:  Um shell bem simples para GNU/Linux
 *
 *        Version:  1.1
 *
 *        Created:  Sex May 6  10:52:25 BRT 2011
 *
 *       Revision:  Ter Jun 28 15:58:57 BRT 2011
 *
 *       Compiler:  gcc
 *
 *       Compiler Flags:  -Wall , obs: não compile com a flag -ansi pois irá inteferir no tratamento de sinais
 *
 *         Author:  Lucas Vinicius Avanço	-	N.USP: 6513538
 *        
 *        Company:	ICMC - USP
 *
 */


#include    <stdlib.h>
#include    <signal.h>
#include    <stdio.h>
#include    <unistd.h>
#include    <sys/types.h>
#include    <sys/wait.h>
#include	<fcntl.h>
#include    <string.h>


void do_execute (char *executavel, char **args);
char * get_exec_path (char *executavel, const char *env_path);
void handler_sigint(int sig);
int internal(char *command, char **args);
int read_command (char **command, char ***args);
int redirect_io (char **args, int type);


/*!
 *
 * \brief Função principal que executa um loop infinito até que seja digitado o comando interno \e exit ou \e CTRL-D.
 * \return \e EXIT_SUCCESS, definido como 0 em stdlib.h
 * \param[in] argc não utilizado
 * \param[in] argv não utilizado
 * \note \e SIGINT, CTRL-C, não finaliza a execução. Caso compile com a flag -ansi o tratamento de sinais não funcionará corretamente
 */
int main ( int argc, char *argv[] )
{
	char * command;
	char **args;
	char *exec_file = NULL;
	const char *path = getenv("PATH");
	int fd_io = 0;
	int old_stdin = dup(STDIN_FILENO);
	int old_stdout = dup(STDOUT_FILENO);

	/* associa a funcao handler_sigint ao tratamento do sinal SIGINT - Ctrl-C */
	signal(SIGINT, handler_sigint);

	for (;;)
	{
		/* mostrar prompt */
		printf("shell$ ");

		/* ler comando digitado: primeiro token eh o executavel, e o restante sao os argumentos */
		int io = read_command(&command, &args);
		if ( io )
		{
			fd_io = redirect_io(args, io);
		}

		/* if ( 'usuario nao digitou apenas [ENTER]' ) */
		if (command[0] != '\0')
		{
			/* verifica se o comando digitado eh um comando interno e executa o comando */
			if (internal(command, args)) continue;

			/* exec_file tera a string com o path completo para o executavel */
			exec_file = get_exec_path(command, path);
			/* if ( 'achou o path para o executavel e tem permissao de leitura sobre o arquivo' ) */
			if ( exec_file != NULL )
				do_execute(exec_file, args);
			else
				printf("comando nao encontrado\n");
		}

		/* restaurar i/o padrao */
		if ( fd_io > 0 )
		{
			close(fd_io);
			dup2(old_stdin, STDIN_FILENO);
			dup2(old_stdout, STDOUT_FILENO);
		}

		if (command != NULL)
		   free(command);
	}

	return EXIT_SUCCESS;
}    /* ----------  end of function main  ---------- */


/*!
 * Faz a leitura do que o usuário digitar para ser interpretado pelo shell até que receba um "\n".
 * Em seguida a string lida é quebrada em tokens onde os separadores são espaços simples ou não, através da função \b strtok() definida em string.h.
 * O primeiro token é o nome do executável e o restante são parâmetros ou \<, \>, \>\> e um nome de arquivo de entrada ou saída.
 *
 * \param[in,out] command alocado com o mesmo tamanho do primeiro token e guarda o nome do executável.
 * \param[in,out] args vetor de strings realocado a cada token recuperado, guarda todos os parametros
 * \return \b 0 caso não haja redirecionamento de entrada/saída \n
 *  \b 1 caso haja redirecionamento de entrada, \< \n
 *  \b 2 caso haja redirecionamento de saída sem append, \> \n
 *  \b 3 caso haja redirecionamento de saída com append, \>\>
 * \sa man 3 strncat \n
 *	man 3 strtok \n
 *	man 3 strcpy \n
 *	man 3 strcmp \n
 *	man 3 malloc
 * \note a última posição do vetor \e args deve apontar para \e NULL pois este vetor será passado para a syscall \b execv(), e a syscall exige isso.
 */
int read_command(char **command, char ***args)
{
	*command = NULL;
	*args = NULL;

	char *token = NULL; /* auxilio para pegar os argumentos de um comando */
	char *input_sec = NULL; /* auxilio para realloc */
	char *command_sec = NULL; /* auxilio para realloc */

	*command = (char *) malloc(sizeof(char));
	(*command)[0] = '\0';

	int cmd_count = 1;  /* controle para alocacao de memoria */
	int args_count = 0; /* controle para realloc de memoria de *args (argumrntos do comando) */
	char c;

	/* leitura de stdin */
	while (1)
	{
		c = fgetc(stdin);
		if ( c == EOF ) /* finaliza o shell quando pega um CTRL-D */
		{
			printf("\n");
			exit(EXIT_SUCCESS);
		}
		switch(c)
		{
			case '\n':
				break;
			default:
				if ( c >= 0 && c <= 255 )
				{
					input_sec = (char *) realloc(*command, ++cmd_count);
					if (input_sec != NULL)
					{
						*command = input_sec;
					}
					strncat(*command, &c, 1);
				}
		}
		if (c == '\n') break;
	}

	if ( (*command)[0] == '\0' )
		return 0;

	/* quebrar toda string lida para pegar os argumentos do comando */
	token = strtok(*command," ");
	while (token != NULL)
	{
		/* alocar memoria para o primeiro argumento */
		*args = (char **) realloc( *args, ++args_count * sizeof(char *));
		(*args)[args_count-1] = (char *) malloc(strlen(token));
		strcpy((*args)[args_count-1], token);
		token = strtok(NULL, " ");
	}
	*args = (char **) realloc( *args, ++args_count * sizeof(char *));
	(*args)[args_count-1] = NULL;

	/* command guarda o executavel, o comando em si */
	if ( (*args)[0] != NULL )
	{
		command_sec = (char *) realloc(*command, strlen((*args)[0]) + 1 );
		if ( command_sec != NULL )
		{
			*command = command_sec;
		}
		strcpy(*command, (*args)[0]);
	}

	if ( token != NULL )
		free(token);

	/*
	caso de redirecionamento de entrada/saida
	ps: args_count conta tambem o nome do executavel
	ps: ultima posicao de args eh NULL
	
	args[args_count-1] == NULL
	args[args_count-2] == ultimo parametro real ( input ou output )
	*/
	if ( args_count > 3 )
	{
		if ( !strcmp( (*args)[args_count-3], "<" )  )
		{
			return 1;
		}
		else if ( !strcmp( (*args)[args_count-3], ">" ) ) 
		{
			return 2;
		}
		else if ( !strcmp( (*args)[args_count-3], ">>" ) )
		{
			return 3;
		}
	}

	return 0;
}


/*!
 * \brief Utiliza a variável de ambiente \b \$PATH para encontrar o caminho completo do executável, caso não seja especificado um arquivo no diretório atual
 * \param[in] executavel string com o nome do arquivo executável
 * \param[in] env_path string com o conteúdo da variável de ambiente \b \$PATH
 * \return string com o caminho completo para o arquivo executável, caso tenha achado o arquivo em algum subdiretório dentro de \b \$PATH e o usuário tenha permissão de leitura sobre o arquivo. Se não foi encontrado o arquivo ou o usuário não tem permissão de acesso, então é retornado \e NULL.
 * \sa man 3 strtok \n
 *	man 3 strcat \n
 *	man 3 malloc \n
 *	man 2 open
 * \note É utilizado \b strtok() definido em string.h com o delimitador de token ":" para obter uma string com um diretório que está em \b \$PATH. Essa string é então concatenada ao nome do arquivo executável, e finalmente essa string é passada para a syscall \b open() para testar permissão de acesso.
 */
char * get_exec_path (char *executavel, const char *env_path)
{
	char *path = (char *) malloc( strlen(env_path) + 1 );
	strcpy(path,env_path);
	char *subpath = NULL;
	char *exec_path = NULL;

	/* execucao de um executavel relativo a direotrio */
	if ( executavel[0] == '.' )
		return executavel;

	subpath = strtok(path, ":");
	while ( subpath != NULL )
	{
		exec_path = (char *) realloc( exec_path, strlen(subpath) + 1 + strlen(executavel)+1); /* "path" + '/' + "executavel" + '\0' */
		exec_path[0] = '\0';
		strcat(exec_path, subpath);
		strcat(exec_path, "/");
		strcat(exec_path, executavel);
		/* tentar abrir o executavel, caso consiga significa que encontrou o executavel e o usuario tem permissao para executa-lo */
		if ( open(exec_path, O_RDONLY) != -1 )
		{
			free(path);
			return exec_path;
		}
		subpath = strtok(NULL, ":");
		free(exec_path);
		exec_path = NULL;
	}
	free(path);
	return NULL; /* buscou pelo executavel em todo PATH, nao encontrou entao retorna NULL */
}


/*!
 * \brief Cria um processo filho através da syscall \b fork() e substitui a imagem do processo filho na memória pelo arquivo executável especificado pelo usuário. Para alterar a imagem do processo filho na memória chama-se a syscall \b execv().
 * \param[in] executavel string com o caminho completo do executável
 * \param[in] args vetor de strings com todos os parâmetros que devem ser passados ao executável
 * \return \e void
 * \sa man 2 fork \n
 *	man 2 execv \n
 *	man 2 waitpid
 * \note o vetor args já vem sem um possível redirecionador de i/o + "nome de arquivo" para entrada ou saída de dados.
 */
void do_execute (char* executavel, char **args)
{
	int status;
	int pid = fork();

	if (pid < 0)
		perror("ERROR");
	else if (pid != 0) /* Processo pai */
	{
		/* -1 espera qualquer filho q terminar primeiro */
		waitpid(-1, &status, 0);
	}
	else
	{
		/* Tenta substituir toda a imagem do filho na memoria */
		/* pelo arquivo indicado no primeiro parametro */
		if (execv(executavel, args) == -1) exit(EXIT_FAILURE);
	}
}


/*!
 * \brief Reconhece alguns comandos internos ao shell
 *
 * \param[in] command string com o nome do arquivo executável
 * \param[in] args vetor de strings com todos os parâmetros que devem ser passados para o executável
 * \return \b 0 caso o executável não seja identificado como um comando interno ao shell \n
 *	\b 1 caso seja um comando interno ao shell
 * \sa man 3 strcmp \n
 *	man 3 getenv \n
 *	man 2 chdir \n
 * \note foram implementados apenas os comandos internos: \e exit e \e cd
 */
int internal(char *command, char **args)
{
	const char *home = getenv("HOME");

	if (!strcmp(command, "exit"))
	{
		exit(EXIT_SUCCESS);
	}
	else if (!strcmp(command, "cd"))
	{
		if (args[1] == NULL)
			chdir(home);
		else if (args[2] == NULL)
		{
			chdir(args[1]);
		}
		return 1;
	}
	return 0;
}


/*!
 * \brief Trata do redirecionamento de entrada/saída( \<, \>, \>\> ) utilizando a syscall \b dup2()
 * \param[in] args vetor de strings com todos os parâmetros lidos em \e read_command()
 * \param[in] type especifica qual tipo de redirecionamento foi digitado pelo usuário: 1 representa \< \b ou 2 representa \> \b ou 3 representa \>\>
 * \return descritor de arquivo do arquivo passado como entrada ou saída de dados
 * \sa man 2 dup2 \n
 *	man 2 open
 * \note old_fd é uma variável auxiliar para armazenar o file descriptor do arquivo passado como entrada ou saída de dados, pois o descritor de arquivo real do arquivo passará a ser uma cópia de \e stdin ou \e stdout, após a chamada a syscall \b dup2(). O antigo file descriptor é então retornado para que o arquivo possa ser fechado na main() após do_execute() já ter finalizado. \n
 *	O vetor args é "podado" antes de retornar. Isso é necessário já que o redirecionador de i/o ( \<, \>, \>\> ) e o nome do arquivo para entrada ou saída de dados, não poderão ser passados para do_execute(), apenas os parâmetros que fazem sentido ao executável devem ser passados em so_execute().
 */
int redirect_io(char **args, int type)
{
	int i, fd, old_fd;
	for ( i=0; args[i] != NULL; i++ );

	switch ( type )
	{
		case 1:
			fd = open(args[i-1], O_RDONLY);
			old_fd = fd; /* guarda o descritor do arquivo */
			if ( dup2(fd, STDIN_FILENO) == -1 ) /* stdin se torna uma copia de fd */
				perror("dup2 error\n");
			break;
		case 2:
			fd = open(args[i-1], O_CREAT | O_WRONLY | O_TRUNC, 0644); /* flag O_TRUNC zera o arquivo caso ele ja exista */
			old_fd = fd; /* guarda o descritor do arquivo */
			if ( dup2(fd, STDOUT_FILENO) == -1 ) /* stdout se torna uma copia de fd */
				perror("dup2 error\n");
			break;
		case 3:
			fd = open(args[i-1], O_CREAT | O_WRONLY | O_APPEND, 0644);
			old_fd = fd; /* guarda o descritor do arquivo */
			if ( dup2(fd, STDOUT_FILENO) == -1 ) /* stdout se torna uma copia de fd */
				perror("dup2 error\n");
			break;
		default:
			break;
	}
	args[i-2] = NULL; /* poda: tira < ou > ou >> mais o nome do arquivo, pois isso nao pode ser passado para do_execute() */
	return old_fd;
}


/*!
 * \brief Função que trata o sinal \b SIGINT recebido pelo processo do shell
 * \param[in] sig sinal a ser tratado - SIGINT
 * \return \e void
 * \sa man 2 signal
 */
void handler_sigint(int sig)
{
	printf("\n");
}
